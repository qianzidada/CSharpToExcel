﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.Data.SqlClient;
using System.Configuration;
using Microsoft.Office.Interop;
using System.Data.OleDb;
using System.Data.ProviderBase;
namespace zuoye1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
    public static string str=ConfigurationManager.ConnectionStrings["aa"].ConnectionString; 
        private void Form1_Load(object sender, EventArgs e)
        { 
            
           
            SqlConnection con = new SqlConnection(str);
            SqlDataAdapter sda = new SqlDataAdapter("select * from Information",con);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            dataGridView1.DataSource=ds.Tables[0];
        
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(str);
            con.Open();
            string Sex = "";
            if (radioButton1.Checked) Sex = radioButton1.Text;
            else Sex = radioButton2.Text;
            string Married = "";
            if (radioButton3.Checked) Married = radioButton3.Text;
            else Married= radioButton4.Text;
            DateTime date = Convert.ToDateTime(dateTimePicker1.Text);
            string ID = textBox1.Text;
            string Name = textBox2.Text;
            string Dname = textBox3.Text;
            //string Birthday = textBox4.Text;
            string Tel = textBox5.Text;
            string Positionname = textBox6.Text;
            string IDcar = textBox8.Text;
            string Adress = textBox7.Text;

            string sql = string.Format("insert into Information(Name,Sex,Dname,Positionname,Tel,Married,Birthday,IDcar,Adress) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')",  Name, Sex, Dname, Positionname, Tel, Married,date,IDcar, Adress);
            SqlCommand cmd = new SqlCommand(sql, con); 
            
            int i = cmd.ExecuteNonQuery();
            if (i > 0)
            {
                MessageBox.Show("ook");
              
            }
            else
            {
                MessageBox.Show("添加失败！");
            }
          
            con.Close();
            Form1_Load(sender, e);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(str);
            con.Open();
            string sex = "";
            if (radioButton1.Checked) sex = radioButton1.Text;
            else sex = radioButton2.Text;
            string Married = "";
            if (radioButton3.Checked) Married = radioButton3.Text;
            else Married = radioButton4.Text;
            DateTime date = Convert.ToDateTime(dateTimePicker1.Text);
            string ID = textBox1.Text;
            string Name = textBox2.Text;
            string Dname = textBox3.Text;
           // string Birthday = textBox4.Text;
            string Tel = textBox5.Text;
            string Positionname = textBox6.Text;
            string IDcar = textBox8.Text;
            string Adress = textBox7.Text;
            string sql = string.Format("delete from Information where ID='{0}'", textBox1.Text);
            SqlCommand cmd = new SqlCommand(sql, con);
            int i = cmd.ExecuteNonQuery();
            if (DialogResult.OK == MessageBox.Show("确定删除?", "tishi", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand))
            {
                if (i > 0)
                {
                    {
                        MessageBox.Show("成功");
                        
                    }

                }
                else
                {
                    MessageBox.Show("删除失败！");
                }
            }
            con.Close();
            Form1_Load(sender, e);
        }


        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(str);
            con.Open();

            string sql = string.Format("update Information set Name='{0}' where ID='{1}'", textBox2.Text, textBox1.Text);
            SqlCommand cmd = new SqlCommand(sql, con);
            int i = cmd.ExecuteNonQuery();
            if (DialogResult.OK == MessageBox.Show("确定修改?", "tishi", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand))
            {
                if (i > 0)
                {
                    MessageBox.Show("成功");
                   
                }
                else
                {
                    MessageBox.Show("修改失败");
                }
            }
            con.Close();
            Form1_Load(sender, e);
        }

        /// <summary>
        /// 查询信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {

            string txt = textBox2.Text;
            //string tj = "";
            //switch (txt)
            //{
            //    case "Name": tj = "Name"; break;
            //}
            SqlConnection con = new SqlConnection(str);
           string sql = string.Format("select * from Information where Name like '%{0}%'", txt);
           
            SqlDataAdapter sda = new SqlDataAdapter(sql, con);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];

            
            con.Close();
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;
            if (i > 0)
            {
                textBox1.Text = dataGridView1.Rows[i].Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.Rows[i].Cells[1].Value.ToString();
                textBox3.Text = dataGridView1.Rows[i].Cells[3].Value.ToString();
                string sex = dataGridView1.Rows[i].Cells[2].Value.ToString();
                if (sex == "男") radioButton1.Checked = true;
                else radioButton2.Checked = true;
                string Married = dataGridView1.Rows[i].Cells[8].Value.ToString();
                if (Married == "未婚") radioButton3.Checked = true;
                else radioButton4.Checked = true;
                //textBox4.Text = dataGridView1.Rows[i].Cells["Birthday"].Value.ToString();
                //dateTimePicker1.Text=dataGridView1.Rows[i].Cells["Birthday"].Value.ToString();
                textBox5.Text = dataGridView1.Rows[i].Cells["Tel"].Value.ToString();
                textBox6.Text = dataGridView1.Rows[i].Cells["Positionname"].Value.ToString();
                textBox8.Text = dataGridView1.Rows[i].Cells["IDcar"].Value.ToString();
                textBox7.Text = dataGridView1.Rows[i].Cells["Adress"].Value.ToString();
               
                

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var open = new OpenFileDialog { Filter = "JPEG文件(*.jpg)|*.jpg|GIF文件(*.gif)|*.gif|BMP文件(*.bmp)|*.bmp" };
            if (open.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image = Image.FromFile(open.FileName);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DataGridViewToExcel(dataGridView1,true);
        }
        //DataGridView数据导出为Excel方法
        public bool DataGridViewToExcel(DataGridView dtgv,bool isShowExcel)
        {
            if (dtgv.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
                excel.Application.Workbooks.Add(true);
                excel.Visible = isShowExcel;
                for (int i = 0; i < dtgv.ColumnCount;i++ )
                {
                    excel.Cells[1, i + 1] = dtgv.Columns[i].HeaderText;
                }
                for (int i = 0; i < dtgv.RowCount - 1;i++ )
                {
                    for (int j = 0; j < dtgv.ColumnCount;j++ )
                    {
                        excel.Cells[i + 2, j + 1] = dtgv[j, i].Value.ToString();
                    }
                }
                return true;
            }
        }
        //Excel数据导入为DataGridView中   
        public bool ExcelToDataGridView(DataGridView dtgv)
        { 
            string MyFileName="";
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Title = "打开excel文件";
            OFD.Filter = "excel03文件(*.xls)|*.xls|excel07文件(*.xlsx)|*.xlsx";
            OFD.InitialDirectory = @"D:\\";
            OFD.RestoreDirectory = true;
            if(OFD.ShowDialog()==DialogResult.OK)
            {
                MyFileName = OFD.FileName;
                dtgv.DataSource = GetExcelData(MyFileName);
                dtgv.DataMember = "[Sheet1$]";
                for (int count = 0; count <= dtgv.Rows.Count - 1;count++ )
                {
                    dtgv.Rows[count].HeaderCell.Value = (count + 1).ToString();
                }
            }
            return true;
        }
        public static DataSet GetExcelData(string str)
        {
            string strCon = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + str + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'";
            OleDbConnection myConn = new OleDbConnection(strCon);
            string strCom = " SELECT * FROM [Sheet1$]";
            myConn.Open();
            OleDbDataAdapter myCommand = new OleDbDataAdapter(strCom, myConn);
            DataSet myDataSet = new DataSet();
            myCommand.Fill(myDataSet, "[Sheet1$]");
            myConn.Close();
            return myDataSet;
        }  
        private void button7_Click(object sender, EventArgs e)
        {
            ExcelToDataGridView(dataGridView1);
        }
    }
}
